### Python project
* use bandit [SAST Analyzer](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html#sast-analyzers-ultimate)
* additional information on the [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) page
* [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

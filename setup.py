import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name = "sudoku-pkg-@evanchatter",
    version = "0.0.1",
    author = "Evan Chan",
    author_email = "evanchatter@gmail.com",
    description = "Sudoku solver",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://gitlab.com/edmond-demo/sandbox/python-example/",
    packages = setuptools.find_packages(),
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires = '>=3.6',
)
